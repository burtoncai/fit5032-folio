﻿namespace FIT5032_Week6.Controllers
{
    public class FormOneViewModel
    {
        public string FirstName { get; internal set; }
        public string LastName { get; internal set; }
    }
}