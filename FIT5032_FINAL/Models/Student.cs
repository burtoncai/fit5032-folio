﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FIT5032_FINAL.Models
{
    using System;
    using System.Collections.Generic;

    public partial class Student
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DOB { get; set; }
        public string Degree { get; set; }

        public virtual Group Group { get; set; }
        public string UserId { get; internal set; }
    }
}